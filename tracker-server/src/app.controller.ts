import { Controller, Post, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { Request } from 'express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  trackedDataReport(@Req() request: Request) {
    const buffers = [];
    request.on('data', (chunk) => {
      buffers.push(chunk);
    });
    request.on('end', () => {
      const bufferData = Buffer.concat(buffers);
      const data = JSON.parse(bufferData.toString());
      this.appService.saveTrackedData(data);
    });
  }
}
